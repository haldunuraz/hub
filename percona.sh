#!/usr/bin/env bash

wget -q https://repo.percona.com/apt/percona-release_0.1-6.$(lsb_release -sc)_all.deb
dpkg -i percona-release_0.1-6.$(lsb_release -sc)_all.deb
apt-get update
export DEBIAN_FRONTEND=noninteractive
sudo -E apt-get -q -y install percona-server-server-5.7 unzip

sed -i 's/= 127.0.0.1/= 0.0.0.0/g' /etc/mysql/my.cnf

service mysql restart


wget -q  http://www2.informatik.uni-freiburg.de/~cziegler/BX/BX-SQL-Dump.zip

unzip BX-SQL-Dump.zip

sed -i 's/TYPE=MyISAM;/ENGINE=InnoDB;/g' BX-Book-Ratings.sql
sed -i 's/BX-Book-Ratings/BXBookRatings/g' BX-Book-Ratings.sql

sed -i 's/TYPE=MyISAM;/ENGINE=InnoDB;/g' BX-Users.sql
sed -i 's/BX-Users/BXUsers/g' BX-Users.sql

sed -i 's/TYPE=MyISAM;/ENGINE=InnoDB;/g' BX-Books.sql
sed -i 's/BX-Books/BXBooks/g' BX-Books.sql

echo "SET NAMES utf8;" > Book-Crossing.sql
cat BX*.sql  >> Book-Crossing.sql

iconv -f ISO8859-14 -t UTF-8//TRANSLIT Book-Crossing.sql -o BookCrossing.sql 

rm Book-Crossing.sql
rm *.zip

sudo mysql -e "CREATE DATABASE BookCrossing CHARACTER SET utf8 COLLATE utf8_unicode_ci;"

sudo mysql  -D BookCrossing < BookCrossing.sql

sudo mysql -e "CREATE USER 'dbadmin'@'%' IDENTIFIED BY 'pass';"
sudo mysql -e "GRANT ALL PRIVILEGES ON * . * TO 'dbadmin'@'%' WITH GRANT OPTION;"
sudo mysql -e "FLUSH PRIVILEGES;"

service ufw stop
