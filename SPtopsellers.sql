DROP PROCEDURE IF EXISTS `TopSellers`;

delimiter //
CREATE DEFINER=CURRENT_USER PROCEDURE `TopSellers`(IN `country` varchar(75))
BEGIN
	INSERT INTO topseller 
        Select * From _temptopseller WHERE _temptopseller.country=country
	    order by RatingCount desc, _temptopseller.Rate Desc
	LIMIT 10;
END
//

delimiter ;
DROP PROCEDURE IF EXISTS `createtopsellers`;


delimiter //
CREATE DEFINER=CURRENT_USER PROCEDURE `createtopsellers`()
BEGIN
  DECLARE location VARCHAR(75);
	DECLARE na BOOLEAN DEFAULT FALSE;
	DECLARE cur CURSOR FOR Select trim(SUBSTRING_INDEX( BXUsers.Location, ',',- 1 )) AS loc FROM BXUsers GROUP BY loc ;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET na := TRUE;

DROP TABLE IF EXISTS _temptopseller;
CREATE TABLE _temptopseller(RatingCount int(4),Rate int(4),Book VARCHAR(255),ISBN varchar(13), country VARCHAR(75), UNIQUE INDEX (ISBN,country) );


INSERT INTO _temptopseller SELECT * from (
	SELECT 
			COUNT( bxr.`Book-Rating` ) AS RatingCount,
			AVG( bxr.`Book-Rating` ) AS AVGRate,
			bxb.`Book-Title` AS Book,
			LOWER(bxb.ISBN),
			TRIM(SUBSTRING_INDEX( bxu.Location, ',',- 1 )) AS loc 
	FROM
		BXBookRatings bxr
	INNER JOIN BXBooks bxb ON bxr.ISBN = bxb.ISBN
	INNER JOIN BXUsers bxu ON bxr.`User-ID` = bxu.`User-ID` 
	GROUP BY
	LOWER(bxb.ISBN),bxb.`Book-Title`,
	TRIM(SUBSTRING_INDEX( bxu.Location, ',',- 1 ))
	) as t;




DROP TABLE IF  EXISTS topseller;
CREATE TABLE  topseller (RatingCount int(4),Rate int(4),Book VARCHAR(255),ISBN varchar(13), country VARCHAR(75) , INDEX (ISBN,country));

OPEN cur ;
countriesloop: LOOP
FETCH cur into location;
	IF na THEN
	LEAVE countriesloop;
    END IF;
    CALL TopSellers(location);
  END LOOP countriesloop;
 
  CLOSE cur;
	
	DROP TABLE IF EXISTS topsellers;
	CREATE TABLE  topsellers (RatingCount int(4),Rate int(4),Book VARCHAR(255),ISBN varchar(13), country VARCHAR(75) , INDEX (ISBN,country));
	INSERT into topsellers Select * from topseller ;
	DROP TABLE IF EXISTS topseller;
	DROP TABLE IF EXISTS _temptopseller;
END
//
delimiter ;